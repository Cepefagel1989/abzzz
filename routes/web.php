<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\EmployeesController;
use App\Http\Controllers\Admin\WorkPositionController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    Inertia::setRootView('login');
    return Inertia::render('Authorization/Login', [
        'canLogin' => Route::has('login'),
    ]);
})->name('home');

Route::group(['prefix' => 'admin', 'middleware' => ['auth:sanctum', 'verified']], function () {
    Route::get('/', DashboardController::class);
    Route::resource('/workpositions', WorkPositionController::class, ['except' => ['show']]);
    Route::get('/employees/search', [EmployeesController::class, 'search'])->name('employees.search');
    Route::get('/employees/has-children', [EmployeesController::class, 'hasChildren'])->name('employees.hasChildren');
    Route::delete('/employees/destroy-chief/{employee}', [EmployeesController::class, 'destroyChief'])->name('employees.destroyChief');
    Route::resource('/employees', EmployeesController::class);
});

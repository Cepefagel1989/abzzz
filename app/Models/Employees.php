<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Kalnoy\Nestedset\Collection;
use Kalnoy\Nestedset\NodeTrait;
use Kalnoy\Nestedset\QueryBuilder;
use NumberFormatter;


/**
 * App\Models\Employees
 *
 * @property int $id
 * @property int $_lft
 * @property int $_rgt
 * @property int|null $parent_id
 * @property int|null $lvl
 * @property int|null $work_position_id
 * @property string $full_name
 * @property string $salary
 * @property string $phone_number
 * @property string $email
 * @property string $date_employment
 * @property string|null $photo
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Employees[] $children
 * @property-read int|null $children_count
 * @property-read mixed $date_employment_format
 * @property-read string $thumb_image
 * @property-read Employees|null $parent
 * @property-read \App\Models\WorkPositions|null $workPosition
 * @method static Collection|static[] all($columns = ['*'])
 * @method static QueryBuilder|Employees ancestorsAndSelf($id, array $columns = [])
 * @method static QueryBuilder|Employees ancestorsOf($id, array $columns = [])
 * @method static QueryBuilder|Employees applyNestedSetScope(?string $table = null)
 * @method static QueryBuilder|Employees countErrors()
 * @method static QueryBuilder|Employees d()
 * @method static QueryBuilder|Employees defaultOrder(string $dir = 'asc')
 * @method static QueryBuilder|Employees descendantsAndSelf($id, array $columns = [])
 * @method static QueryBuilder|Employees descendantsOf($id, array $columns = [], $andSelf = false)
 * @method static QueryBuilder|Employees fixSubtree($root)
 * @method static QueryBuilder|Employees fixTree($root = null)
 * @method static Collection|static[] get($columns = ['*'])
 * @method static QueryBuilder|Employees getNodeData($id, $required = false)
 * @method static QueryBuilder|Employees getPlainNodeData($id, $required = false)
 * @method static QueryBuilder|Employees getTotalErrors()
 * @method static QueryBuilder|Employees hasChildren()
 * @method static QueryBuilder|Employees hasParent()
 * @method static QueryBuilder|Employees isBroken()
 * @method static QueryBuilder|Employees leaves(array $columns = [])
 * @method static QueryBuilder|Employees makeGap(int $cut, int $height)
 * @method static QueryBuilder|Employees moveNode($key, $position)
 * @method static QueryBuilder|Employees newModelQuery()
 * @method static QueryBuilder|Employees newQuery()
 * @method static QueryBuilder|Employees orWhereAncestorOf(bool $id, bool $andSelf = false)
 * @method static QueryBuilder|Employees orWhereDescendantOf($id)
 * @method static QueryBuilder|Employees orWhereNodeBetween($values)
 * @method static QueryBuilder|Employees orWhereNotDescendantOf($id)
 * @method static QueryBuilder|Employees query()
 * @method static QueryBuilder|Employees rebuildSubtree($root, array $data, $delete = false)
 * @method static QueryBuilder|Employees rebuildTree(array $data, $delete = false, $root = null)
 * @method static QueryBuilder|Employees reversed()
 * @method static QueryBuilder|Employees root(array $columns = [])
 * @method static QueryBuilder|Employees whereAncestorOf($id, $andSelf = false, $boolean = 'and')
 * @method static QueryBuilder|Employees whereAncestorOrSelf($id)
 * @method static QueryBuilder|Employees whereCreatedAt($value)
 * @method static QueryBuilder|Employees whereDateEmployment($value)
 * @method static QueryBuilder|Employees whereDescendantOf($id, $boolean = 'and', $not = false, $andSelf = false)
 * @method static QueryBuilder|Employees whereDescendantOrSelf(string $id, string $boolean = 'and', string $not = false)
 * @method static QueryBuilder|Employees whereEmail($value)
 * @method static QueryBuilder|Employees whereFullName($value)
 * @method static QueryBuilder|Employees whereId($value)
 * @method static QueryBuilder|Employees whereIsAfter($id, $boolean = 'and')
 * @method static QueryBuilder|Employees whereIsBefore($id, $boolean = 'and')
 * @method static QueryBuilder|Employees whereIsLeaf()
 * @method static QueryBuilder|Employees whereIsRoot()
 * @method static QueryBuilder|Employees whereLft($value)
 * @method static QueryBuilder|Employees whereLvl($value)
 * @method static QueryBuilder|Employees whereNodeBetween($values, $boolean = 'and', $not = false)
 * @method static QueryBuilder|Employees whereNotDescendantOf($id)
 * @method static QueryBuilder|Employees whereParentId($value)
 * @method static QueryBuilder|Employees wherePhoneNumber($value)
 * @method static QueryBuilder|Employees wherePhoto($value)
 * @method static QueryBuilder|Employees whereRgt($value)
 * @method static QueryBuilder|Employees whereSalary($value)
 * @method static QueryBuilder|Employees whereUpdatedAt($value)
 * @method static QueryBuilder|Employees whereWorkPositionId($value)
 * @method static QueryBuilder|Employees withDepth(string $as = 'depth')
 * @method static QueryBuilder|Employees withoutRoot()
 * @mixin \Eloquent
 */
class Employees extends Model
{
    use HasFactory, NodeTrait;

    protected $guarded = [];
    protected $appends = ['thumb_image', 'date_employment_format', 'chief'];

    protected static function booted()
    {
        static::created(function (Employees $employee) {
            $employee->update(['lvl' => $employee->ancestors()->count() + 1]);
        });
    }

    public function workPosition(): BelongsTo
    {
        return $this->belongsTo(WorkPositions::class);
    }

    public function getChiefAttribute()
    {
        return $this->parent;
    }

    public function getDateEmploymentAttribute($value)
    {
        return date('y-m-d', strtotime($value));
    }

    public function getDateEmploymentFormatAttribute($value)
    {
        return date('d.m.y', strtotime($this->date_employment));
    }

    public function getCreatedAtAttribute($value)
    {
        return date('d.m.y H:i:s', strtotime($value));
    }

    public function getUpdatedAtAttribute($value)
    {
        return date('d.m.y H:i:s', strtotime($value));

    }

    public function getSalaryAttribute($value): string
    {
        $fmt = new NumberFormatter('en_EN', NumberFormatter::DEFAULT_STYLE);
        return '$' . $fmt->formatCurrency($value, 'USD');
    }

    public function getThumbImageAttribute($value): string
    {
        if (!$this->photo) {
            return asset('images/user-avatar.jpg');
        }
        return asset('storage/photos/thumbs/' . basename($this->photo));
    }
}

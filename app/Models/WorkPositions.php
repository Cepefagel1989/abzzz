<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\WorkPositions
 *
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Employees[] $employees
 * @property-read int|null $employees_count
 * @method static Builder|WorkPositions newModelQuery()
 * @method static Builder|WorkPositions newQuery()
 * @method static Builder|WorkPositions query()
 * @method static Builder|WorkPositions whereCreatedAt($value)
 * @method static Builder|WorkPositions whereId($value)
 * @method static Builder|WorkPositions whereName($value)
 * @method static Builder|WorkPositions whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static Builder|WorkPositions sortable($defaultParameters = null)
 */
class WorkPositions extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function employees(): HasMany
    {
        return $this->hasMany(Employees::class, 'work_position_id', 'id');
    }

    public function getCreatedAtAttribute($value)
    {
        return date('d.m.y H:i:s', strtotime($value));
    }

    public function getUpdatedAtAttribute($value)
    {
        return date('d.m.y H:i:s', strtotime($value));
    }
}

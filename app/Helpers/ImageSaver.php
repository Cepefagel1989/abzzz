<?php


namespace App\Helpers;

use Intervention\Image\Facades\Image;


class ImageSaver
{
    /**
     * @param null $instance
     */
    public function upload($field = null)
    {
        $request = request();
        if ($request->hasFile($field)) {
            $uploaded_file = $request->{$field};
            $extension = $uploaded_file->extension();
            $file_name = \Str::uuid() . '.' . $extension;
            $validated['photo'] = $uploaded_file->storeAs('public/photos', $file_name);
            $img = Image::make(storage_path('app/public/photos/' . $file_name))->fit(300, 300)->stream();
            \Storage::disk('public')->put('photos/thumbs/' . $file_name, $img);

            return $file_name;
        }
    }
}

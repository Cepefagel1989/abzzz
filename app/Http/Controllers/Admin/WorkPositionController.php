<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\WorkPositionRequest;
use App\Models\WorkPositions;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;

class WorkPositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Inertia\Response
     */
    public function index(Request $request): Response
    {
        $query = WorkPositions::query();

        if ($request->filled('search')) {
            $search = $request->input('search', '');
            $query->where('name', 'like', "%$search%");
        }
        if ($request->filled('sort') and $request->filled('direction')) {
            $sortColumn = $request->input('sort');
            $direction = $request->input('direction', 'asc');
            $query->orderBy($sortColumn, $direction);
        }

        $data = $query->paginate(5)->onEachSide(3)->appends([
            'sort' => $request->filled('sort') ? $request->input('sort') : '',
            'direction' => $request->filled('direction') ? $request->input('direction') : '',
            'search' => $request->filled('search') ? $request->input('search') : '',
        ]);

        return Inertia::render('WorkPositions/Index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(): Response
    {
        return Inertia::render('WorkPositions/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param WorkPositionRequest $request
     * @return RedirectResponse
     */
    public function store(WorkPositionRequest $request): RedirectResponse
    {
        $data = WorkPositions::create($request->validated());
        $message = "Record " . $data->name . " successfully created.";
        return redirect()->route('workpositions.index')->with('message', $message);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param WorkPositions $workposition
     * @return Response
     */
    public function edit(WorkPositions $workposition): Response
    {
        return Inertia::render('WorkPositions/Edit', ['data' => $workposition]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param WorkPositionRequest $request
     * @param WorkPositions $workposition
     * @return RedirectResponse
     */
    public function update(WorkPositionRequest $request, WorkPositions $workposition): RedirectResponse
    {
        $workposition->update($request->validated());
        $message = "Record $workposition->name has been successfully updated.";
        return redirect()->route('workpositions.index')->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param WorkPositions $workposition
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(WorkPositions $workposition): RedirectResponse
    {
        $name = $workposition->name;
        $workposition->delete();
        return redirect()->back()->with('message', "Position $name successfully deleted!");
    }
}

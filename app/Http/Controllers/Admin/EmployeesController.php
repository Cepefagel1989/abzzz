<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeesRequest;
use App\Models\Employees;
use App\Models\WorkPositions;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Inertia\Inertia;
use App\Helpers\ImageSaver;

class EmployeesController extends Controller
{
    const MAX_LEVEL = 5;

    private $imageSaver;

    public function __construct(ImageSaver $imageSaver)
    {
        $this->imageSaver = $imageSaver;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Inertia\Response
     */
    public function index(Request $request): \Inertia\Response
    {
        $query = Employees::query()->with('workPosition');

        if ($request->filled('search')) {
            $search = $request->input('search', '');
            $query->where('full_name', 'like', "%$search%")
                ->orWhere('email', 'like', "%$search%");
        }

        if ($request->filled('sort') and $request->filled('direction')) {
            $sortColumn = $request->input('sort');
            $direction = $request->input('direction', 'asc');
            $query->orderBy($sortColumn, $direction);
        }

        $serverData = $query->paginate(10)->onEachSide(3)->appends([
            'sort' => $request->filled('sort') ? $request->input('sort') : '',
            'direction' => $request->filled('direction') ? $request->input('direction') : '',
            'search' => $request->filled('search') ? $request->input('search') : ''
        ]);

        return Inertia::render('Employees/Index', compact('serverData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     */
    public function create(): \Inertia\Response
    {
        $serverData = [];
        $serverData['workPositions'] = $this->getWorkPositions();

        return Inertia::render('Employees/Create', compact('serverData'));
    }

    public function search(Request $request): \Illuminate\Support\Collection
    {
        $query = Employees::select('id', 'full_name', 'photo')
            ->where('lvl', '<', self::MAX_LEVEL)
            ->orderBy('full_name');

        if ($request->filled('q')) {
            $q = $request->input('q');
            $query->where('full_name', 'like', "%$q%");
        }

        if ($request->filled('exclude')) {
            $exclude = $request->input('exclude');
            $childrenIds = Employees::descendantsOf($exclude)->pluck('id');
            $query->where('id', '!=', $exclude)
                ->whereNotIn('id', $childrenIds);
        }

        return $query->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EmployeesRequest $request
     * @return RedirectResponse
     */
    public function store(EmployeesRequest $request): RedirectResponse
    {
        $validated = $request->validated();
        $validated['photo'] = $this->imageSaver->upload('photo');
        $data = Employees::create($validated);
        $message = "Record " . $data->full_name . " successfully created.";
        return redirect()->route('employees.index')->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param Employees $employee
     * @return Response
     */
    public function show(Employees $employee)
    {
        dd($employee->ancestors()->get());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Inertia\Response
     */
    public function edit($id): \Inertia\Response
    {
        $serverData['employee'] = Employees::find($id);
        $serverData['workPositions'] = $this->getWorkPositions();

        return Inertia::render('Employees/Edit', compact('serverData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EmployeesRequest $request
     * @param Employees $employee
     * @return RedirectResponse
     */
    public function update(EmployeesRequest $request, Employees $employee): RedirectResponse
    {
        $validated = $request->validated();
        if ($request->hasFile('photo') and $request->file('photo')->isValid()) {
            $validated['photo'] = $this->imageSaver->upload('photo');
        } else {
            unset($validated['photo']);
        }
        $employee->update($validated);
        \DB::table($employee->getTable())
            ->where('id', $employee->id)
            ->update(['lvl' => $employee->ancestors()->count() + 1]);

        $message = "Employee $employee->full_name has been successfully updated.";
        return redirect()->route('employees.index')->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Employees $employee
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Employees $employee)
    {
        $employee->delete();
        return redirect()->back()->with('message', "Position $employee->full_name successfully deleted!");
    }

    public function destroyChief(Request $request, Employees $employee): RedirectResponse
    {
        $newParentNote = Employees::find($request->input('new_parent_id'));
        $employee->children->map(function (Employees $node) use ($newParentNote) {
            $node->appendToNode($newParentNote)->save();
        });
        $message = "Employee $employee->full_name has been successfully deleted.";
        $employee->delete();
        return redirect()->route('employees.index')->with('message', $message);
    }

    public function hasChildren(Request $request): \Illuminate\Http\JsonResponse
    {
        $employee = Employees::findOrFail($request->input('id'));
        $children = $employee->children()->count();
        return \response()->json([
            'parent' => $employee,
            'children' => $employee->children,
            'hasChildren' => (bool)$children,
        ]);
    }

    protected function getWorkPositions(): Collection
    {
        return WorkPositions::query()->orderBy('name')->get()->map(function ($item, $key) {
            return ['value' => $item->id, 'text' => $item->name];
        });
    }
}

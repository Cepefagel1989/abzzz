<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeesRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'full_name' => 'required|min:2|max:256',
            'parent_id' => 'nullable',
            'phone_number' => 'required',
            'email' => 'required|email',
            'work_position_id' => 'required',
            'salary' => 'required|integer|between:0,500000',
            'date_employment' => 'date',
            'photo' => 'nullable|mimes:jpeg,jpg,png|image|dimensions:min_width=300,min_height=300|max:5120'
        ];
    }
}

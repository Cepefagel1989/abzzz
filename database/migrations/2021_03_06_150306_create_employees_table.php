<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->nestedSet();
            $table->integer('lvl')->unsigned()->nullable();
//            $table->integer('chief_id')->unsigned()->nullable();
            $table->integer('work_position_id')->unsigned()->nullable();
            $table->string('full_name', 256);
            $table->integer('salary')->default(0);
            $table->string('phone_number', 256);
            $table->string('email', 256);
            $table->dateTime('date_employment')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('photo')->nullable();
            $table->timestamps();
//            $table->foreign('chief_id')->references('id')->on('employees');
            $table
                ->foreign('work_position_id')
                ->references('id')
                ->on('work_positions')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}

<?php

namespace Database\Factories;

use App\Models\Employees;
use App\Models\WorkPositions;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Employees::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $faker = \Faker\Factory::create('ru_RU');
        $workPosition = WorkPositions::all()->pluck('id');
        $fullName = $faker->firstNameMale . ' ' . $faker->lastName;
        return [
            'full_name' => $fullName,
            'work_position_id' => $faker->randomElement($workPosition),
            'salary' => $faker->numberBetween(0, 500000),
            'email' => $faker->email,
            'phone_number' => $faker->phoneNumber,
            'date_employment' => $faker->dateTime(),
            'photo' => ''
        ];
    }
}

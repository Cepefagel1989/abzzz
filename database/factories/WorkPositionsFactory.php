<?php

namespace Database\Factories;

use App\Models\WorkPositions;
use Illuminate\Database\Eloquent\Factories\Factory;

class WorkPositionsFactory extends Factory
{
    protected $workPositionsArray = [
        'Administrative Assistant', 'Receptionist', 'Office Manager', 'Auditing Clerk', 'Bookkeeper',
        'Account Executive', 'Branch Manager', 'Business Manager', 'Quality Control Coordinator', 'PHP', 'Frontend Developer',
        'Backend Developer', 'Python Developer', 'Administrative Manager', 'Chief Executive Officer', 'Business Analyst',
        'Risk Manager', 'Human Resources', 'Office Assistant', 'Secretary', 'Office Clerk', 'File Clerk',
        'Account Collector', 'Administrative Specialist', 'Executive Assistant', 'Program Administrator',
        'Program Manager', 'Administrative Analyst', 'Data Entry'];

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = WorkPositions::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->unique()->randomElement($this->workPositionsArray)
        ];
    }
}

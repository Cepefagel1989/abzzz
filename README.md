Laravel 8 ● InertiaJS ● MySQL ● PHP ^7.3|^8.0 ● Docker ● Docker Compose 

## Установка
``git clone https://Cepefagel1989@bitbucket.org/Cepefagel1989/abzzz.git``

``composer install && mv .env.example .env``

``php artisan key:generate --ansi``

``./vendor/bin/sail up -d``

``./vendor/bin/sail artisan storage:link``

``./vendor/bin/sail artisan migrate:fresh --seed``

```./vendor/bin/sail npm install && ./vendor/bin/sail npm run dev```

### Login page
![Login Page](./Images/Login.png)

### Employees List
![Employees List](./Images/Employees.png)

### Employee Add
![Employees List](./Images/EmployeeAdd.png)

### Work Positions
![Employees List](./Images/WorkPositions.png)
